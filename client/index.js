import React from 'react';
import ReactDOM from 'react-dom';
import Footer from './components/footer';
import Home from './components/home';
import About from './components/about';
import NavBar from './components/NavBar';
import Contact from './components/contact';
import './stylesheets/index.scss';
import { Router, Route, Link, browserHistory, IndexRoute } from 'react-router'
import Data from './components/Data';
import Clients from './components/Clients';
import Tools from './components/Tools';

class App extends React.Component{

  constructor(props){
    super(props)
    this.state={
      isNavOpen: false
    }
  }

  toggleNav(e){
    this.setState({isNavOpen: !this.state.isNavOpen})
  }

  render(){
    return(
      <div>
        <section id="header">
          <div className="title">
            <Link to="/"><h2>Fractal Strategies</h2></Link>
          </div>
          <div className="hamburger">
            <button onClick={this.toggleNav.bind(this,'open')}>☰</button>
          </div>
        </section>
        <div id="detail">{this.props.children}</div>
        <Footer/>
        {
          this.state.isNavOpen ?
        <div className={`navbar ${this.state.isNavOpen ? 'open' : ''}`}>
            <NavBar closeNav={this.toggleNav.bind(this,'close')}/>
        </div>
        :
        null
        }
      </div>
    )
  }
}

ReactDOM.render(
  <Router history={browserHistory}>
    <Route path="/" component={App}>
      <IndexRoute component={Home} />
      <Route path="about" component={About}/>
      <Route path="contact" component={Contact} />
      <Route path="clients" component={Clients} />
      <Route path="data" component={Data} />
      <Route path="tools" component={Tools} />
    </Route>
  </Router>
  ,document.getElementById('main'))
