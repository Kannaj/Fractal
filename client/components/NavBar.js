import React from 'react';
import { Link} from 'react-router'

class NavBar extends React.Component{
  render(){
    console.log(this.props)
    return(
      <div className="Navbar">

        <div className="closeNav">
           <button onClick={() => this.props.closeNav()}>X</button>
        </div>

        <div className="NavLinks">
           <Link to="/" onClick={() => this.props.closeNav()}>Home</Link>
           <Link to="/about" onClick={() => this.props.closeNav()}>About</Link>
           <Link to="/data" onClick={() => this.props.closeNav()}>Data</Link>
           <Link to="/clients" onClick={() => this.props.closeNav()}>Clients</Link>
           <Link to="/tools" onClick={() => this.props.closeNav()}>Tools</Link>
           <Link to="/contact" onClick={() => this.props.closeNav()}>Contact us</Link>
        </div>

      </div>
    )
  }
}

export default NavBar;
