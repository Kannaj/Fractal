import React from 'react';

class Home extends React.Component{
  render(){
    return(
      <div className="homepage">
        <section className="placeholder">
          <div className="title">
            <h1>Fractal Strategies</h1>
            <h3>Customized data analysis for your business</h3>
          </div>
        </section>
        <section className="placeholder">
          <div className="title">
            <h2>Find Patterns In Your Business Data</h2>
          </div>
        </section>
        <section className="placeholder">
          <div className="title">
            <h2>Incorporate Public Data</h2>
          </div>
        </section>
        <section className="placeholder">
          <div className="title">
            <h2>Business Focused Visualization</h2>
          </div>
        </section>
      </div>
    )
  }
}

export default Home;
