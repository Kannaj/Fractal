import React from 'react';

class Contact extends React.Component{
  render(){
    return(
      <div className="homepage">
        <section className="placeholder">
          <div className="title">
            <h1> Contact us</h1>
            <h3> 0118 999 881 99 9119 7253 </h3>
          </div>
        </section>
      </div>
    )
  }
}

export default Contact;
