import React from 'react';
import { Link} from 'react-router'


class Footer extends React.Component{
  render(){
    return(
      <section id="footer">
        <div className="nav-links">
          <Link to="/" onClick={() => this.props.closeNav()}>Home</Link>
          <Link to="/about" onClick={() => this.props.closeNav()}>About</Link>
          <Link to="/data" onClick={() => this.props.closeNav()}>Data</Link>
          <Link to="/clients" onClick={() => this.props.closeNav()}>Clients</Link>
          <Link to="/tools" onClick={() => this.props.closeNav()}>Tools</Link>
          <Link to="/contact" onClick={() => this.props.closeNav()}>Contact us</Link>
        </div>
      </section>
    )
  }
}

export default Footer;
