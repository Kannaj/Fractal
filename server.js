var express = require('express')
var serveStatic =  require('serve-static')
var path = require('path')

const app = express();

app.use(serveStatic('public'))

app.get('*', function (req, res) {
  res.sendFile(path.join(__dirname, 'public/index.html'))
})

app.listen(4000,function(){
  console.log('App listening on 4000')
})
